program fractais;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  constantes in 'src\programa\constantes.pas',
  terminais in 'src\expressoes\terminais.pas',
  operadores_unarios in 'src\expressoes\operadores_unarios.pas',
  operadores_binarios in 'src\expressoes\operadores_binarios.pas',
  funcoes in 'src\expressoes\funcoes.pas',
  execucoes in 'src\programa\execucoes.pas',
  FractalLotus.Fractais.Expressoes.Expressao in 'src\expressoes\FractalLotus.Fractais.Expressoes.Expressao.pas',
  FractalLotus.Fractais.Expressoes.Sintaxe in 'src\expressoes\FractalLotus.Fractais.Expressoes.Sintaxe.pas',
  FractalLotus.Fractais.FuncaoHolomorfa in 'src\programa\FractalLotus.Fractais.FuncaoHolomorfa.pas',
  FractalLotus.Fractais.Config in 'src\programa\FractalLotus.Fractais.Config.pas',
  FractalLotus.Fractais.BaciaNewton in 'src\programa\FractalLotus.Fractais.BaciaNewton.pas';

begin
  try
    case ParamCount of
      0: executar;
      1: executar(ParamStr(1));
      else imprimirErro;
    end;
  except
    imprimirErro;
  end;

  ReportMemoryLeaksOnShutdown := True;
  IsConsole                   := False;
end.
