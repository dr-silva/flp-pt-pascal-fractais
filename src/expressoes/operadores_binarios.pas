unit operadores_binarios;

interface
uses
  FractalLotus.Fundacao.Matematica.Complexo,
  FractalLotus.Fractais.Expressoes.Expressao,
  constantes;

type
  TOperadorBinario = class abstract (TExpressao)
  strict private
    fOperador: Char;
    fDireita ,
    fEsquerda: TExpressao;
  protected
    function clonar     : TExpressao; overload; override;
    function derivar    : TExpressao; overload; override;
    function simplificar: TExpressao; overload; override;
  strict protected
    function calcular   (const esquerda, direita: TComplexo ): TComplexo;                         virtual; abstract;
    function clonar     (const esquerda, direita: TExpressao): TExpressao; reintroduce; overload; virtual; abstract;
    function derivar    (const esquerda, direita: TExpressao): TExpressao; reintroduce; overload; virtual; abstract;
    function simplificar(const esquerda, direita: TExpressao): TExpressao; reintroduce; overload; virtual; abstract;
  public
    constructor Create(const operador: Char; const esquerda, direita: TExpressao);
    destructor  Destroy; override;

    function ToString: string; override;

    function valorar(const x: TComplexo): TComplexo; override;
  end;

  TOperadorAdicao = class (TOperadorBinario)
  strict protected
    function calcular   (const esquerda, direita: TComplexo ): TComplexo;  override;
    function clonar     (const esquerda, direita: TExpressao): TExpressao; override;
    function derivar    (const esquerda, direita: TExpressao): TExpressao; override;
    function simplificar(const esquerda, direita: TExpressao): TExpressao; override;
  public
    constructor Create(const esquerda, direita: TExpressao);
  end;

  TOperadorSubtracao = class (TOperadorBinario)
  strict protected
    function calcular   (const esquerda, direita: TComplexo ): TComplexo;  override;
    function clonar     (const esquerda, direita: TExpressao): TExpressao; override;
    function derivar    (const esquerda, direita: TExpressao): TExpressao; override;
    function simplificar(const esquerda, direita: TExpressao): TExpressao; override;
  public
    constructor Create(const esquerda, direita: TExpressao);
  end;

  TOperadorMultiplicacao = class (TOperadorBinario)
  strict protected
    function calcular   (const esquerda, direita: TComplexo ): TComplexo;  override;
    function clonar     (const esquerda, direita: TExpressao): TExpressao; override;
    function derivar    (const esquerda, direita: TExpressao): TExpressao; override;
    function simplificar(const esquerda, direita: TExpressao): TExpressao; override;
  public
    constructor Create(const esquerda, direita: TExpressao);
  end;

  TOperadorDivisao = class (TOperadorBinario)
  strict protected
    function calcular   (const esquerda, direita: TComplexo ): TComplexo;  override;
    function clonar     (const esquerda, direita: TExpressao): TExpressao; override;
    function derivar    (const esquerda, direita: TExpressao): TExpressao; override;
    function simplificar(const esquerda, direita: TExpressao): TExpressao; override;
  public
    constructor Create(const esquerda, direita: TExpressao);
  end;

  TOperadorExponenciacao = class (TOperadorBinario)
  strict private
    function derivarNumero(const esquerda, direita: TExpressao): TExpressao;
    function derivarPadrao(const esquerda, direita: TExpressao): TExpressao;
  strict protected
    function calcular   (const esquerda, direita: TComplexo ): TComplexo;  override;
    function clonar     (const esquerda, direita: TExpressao): TExpressao; override;
    function derivar    (const esquerda, direita: TExpressao): TExpressao; override;
    function simplificar(const esquerda, direita: TExpressao): TExpressao; override;
  public
    constructor Create(const esquerda, direita: TExpressao);
  end;

implementation
uses
  terminais,
  operadores_unarios,
  funcoes;

type
  TFriend = class(TExpressao)
  end;

{ TOperadorBinario }

function TOperadorBinario.clonar: TExpressao;
begin
  Result := clonar(TFriend(fEsquerda).clonar, TFriend(fDireita).clonar);
end;

constructor TOperadorBinario.Create(const operador: Char; const esquerda,
  direita: TExpressao);
begin
  fOperador := operador;
  fDireita  := direita;
  fEsquerda := esquerda;
end;

function TOperadorBinario.derivar: TExpressao;
begin
  Result := derivar(TFriend(fEsquerda).clonar, TFriend(fDireita).clonar);
end;

destructor TOperadorBinario.Destroy;
begin
  fDireita .Free;
  fEsquerda.Free;

  inherited Destroy;
end;

function TOperadorBinario.simplificar: TExpressao;
var
  bkpDireita ,
  bkpEsquerda: TExpressao;
begin
  bkpDireita  := fDireita;
  bkpEsquerda := fEsquerda;
  fDireita    := TFriend(fDireita ).simplificar;
  fEsquerda   := TFriend(fEsquerda).simplificar;

  if (bkpDireita <> fDireita) then
    bkpDireita.Free;
  if (bkpEsquerda <> fEsquerda) then
    bkpEsquerda.Free;

  if ((fEsquerda is TNumero) and (fDireita is TNumero)) then
    Result := TNumero.Create
    (
      calcular(TNumero(fEsquerda).valor, TNumero(fDireita).valor)
    )
  else if ((fEsquerda is TNumero) or (fDireita is TNumero)) then
    Result := simplificar(fEsquerda, fDireita)
  else
    Result := Self;
end;

function TOperadorBinario.ToString: string;
begin
  Result := '(' + fEsquerda.ToString +
            ' ' + fOperador +
            ' ' + fDireita.ToString + ')';
end;

function TOperadorBinario.valorar(const x: TComplexo): TComplexo;
begin
  Result := calcular(fEsquerda.valorar(x), fDireita.valorar(x));
end;

{ TOperadorAdicao }

function TOperadorAdicao.calcular(const esquerda,
  direita: TComplexo): TComplexo;
begin
  Result := esquerda + direita;
end;

function TOperadorAdicao.clonar(const esquerda,
  direita: TExpressao): TExpressao;
begin
  Result := TOperadorAdicao.Create(esquerda, direita);
end;

constructor TOperadorAdicao.Create(const esquerda, direita: TExpressao);
begin
  inherited Create('+', esquerda, direita);
end;

function TOperadorAdicao.derivar(const esquerda,
  direita: TExpressao): TExpressao;
var
  novaDireita ,
  novaEsquerda: TExpressao;
begin
  novaDireita  := TFriend(direita ).derivar;
  novaEsquerda := TFriend(esquerda).derivar;

  Result := TOperadorAdicao.Create(novaEsquerda, novaDireita);

  direita .Free;
  esquerda.Free;
end;

function TOperadorAdicao.simplificar(const esquerda,
  direita: TExpressao): TExpressao;
begin
  if ((esquerda is TNumero) and (TNumero(esquerda).eZero)) then
    Result := TFriend(direita).clonar
  else if ((direita is TNumero) and (TNumero(direita).eZero)) then
    Result := TFriend(esquerda).clonar
  else
    Result := Self;
end;

{ TOperadorSubtracao }

function TOperadorSubtracao.calcular(const esquerda,
  direita: TComplexo): TComplexo;
begin
  Result := esquerda - direita;
end;

function TOperadorSubtracao.clonar(const esquerda,
  direita: TExpressao): TExpressao;
begin
  Result := TOperadorSubtracao.Create(esquerda, direita);
end;

constructor TOperadorSubtracao.Create(const esquerda, direita: TExpressao);
begin
  inherited Create('-', esquerda, direita);
end;

function TOperadorSubtracao.derivar(const esquerda,
  direita: TExpressao): TExpressao;
begin
  Result := TOperadorSubtracao.Create(TFriend(esquerda).derivar, TFriend(direita).derivar);

  direita .Free;
  esquerda.Free;
end;

function TOperadorSubtracao.simplificar(const esquerda,
  direita: TExpressao): TExpressao;
begin
  if ((esquerda is TNumero) and (TNumero(esquerda).eZero)) then
    Result := TOperadorNegativo.Create( TFriend(direita).clonar )
  else if ((direita is TNumero) and (TNumero(direita).eZero)) then
    Result := TFriend(esquerda).clonar
  else
    Result := Self;
end;

{ TOperadorMultiplicacao }

function TOperadorMultiplicacao.calcular(const esquerda,
  direita: TComplexo): TComplexo;
begin
  Result := esquerda * direita;
end;

function TOperadorMultiplicacao.clonar(const esquerda,
  direita: TExpressao): TExpressao;
begin
  Result := TOperadorMultiplicacao.Create(esquerda, direita);
end;

constructor TOperadorMultiplicacao.Create(const esquerda, direita: TExpressao);
begin
  inherited Create('*', esquerda, direita);
end;

function TOperadorMultiplicacao.derivar(const esquerda,
  direita: TExpressao): TExpressao;
var
  backup ,
  diffEsq,
  diffDir,
  novaEsq,
  novaDir: TExpressao;
begin
  diffEsq := TFriend(esquerda)     .derivar;
  diffDir := TFriend(direita)      .derivar;
  novaEsq := TOperadorMultiplicacao.Create(diffEsq,  direita);
  novaDir := TOperadorMultiplicacao.Create(esquerda, diffDir);
  backup  := TOperadorAdicao       .Create(novaEsq,  novaDir);
  Result  := TFriend(backup)       .simplificar;

  if (Result <> backup) then
    backup.Free;
end;

function TOperadorMultiplicacao.simplificar(const esquerda,
  direita: TExpressao): TExpressao;
var
  numero: TNumero;
begin
  if (esquerda is TNumero) then
    numero := TNumero(esquerda)
  else
    numero := TNumero(direita);

  if numero.eZero then
    Result := TNumero.Create(TComplexo.ZERO)
  else if numero.eUm then
  begin
    if (esquerda is TNumero) then
      Result := TFriend(direita).clonar
    else
      Result := TFriend(esquerda).clonar;
  end
  else
    Result := Self;
end;

{ TOperadorDivisao }

function TOperadorDivisao.calcular(const esquerda,
  direita: TComplexo): TComplexo;
begin
  Result := esquerda / direita;
end;

function TOperadorDivisao.clonar(const esquerda,
  direita: TExpressao): TExpressao;
begin
  Result := TOperadorDivisao.Create(esquerda, direita);
end;

constructor TOperadorDivisao.Create(const esquerda, direita: TExpressao);
begin
  inherited Create('/', esquerda, direita);
end;

function TOperadorDivisao.derivar(const esquerda,
  direita: TExpressao): TExpressao;
var
  backup,
  diffEsq,   diffDir,
  novaEsq,   novaDir,
  potencia,  expoente,
  numerador, denominador: TExpressao;
begin
  expoente    := TNumero               .Create(TComplexo.Create(2, 0));
  potencia    := TFriend(direita )     .clonar;
  diffEsq     := TFriend(esquerda)     .derivar;
  diffDir     := TFriend(direita )     .derivar;
  novaEsq     := TOperadorMultiplicacao.Create(diffEsq,   direita);
  novaDir     := TOperadorMultiplicacao.Create(esquerda,  diffDir);
  numerador   := TOperadorSubtracao    .Create(novaEsq,   novaDir);
  denominador := TOperadorExponenciacao.Create(potencia,  expoente);
  backup      := TOperadorDivisao      .Create(numerador, denominador);
  Result      := TFriend(backup)       .simplificar;

  if (Result <> backup) then
    backup.Free;
end;

function TOperadorDivisao.simplificar(const esquerda,
  direita: TExpressao): TExpressao;
var
  numero: TNumero;
begin
  if (esquerda is TNumero) then
    numero := TNumero(esquerda)
  else
    numero := TNumero(direita);

  if numero.eZero then
  begin
    if (esquerda is TNumero) then
      Result := TNumero.Create(TComplexo.ZERO)
    else
      Result := TNumero.Create(TComplexo.NaN);
  end
  else if ((direita is TNumero) and numero.eUm) then
    Result := TFriend(esquerda).clonar
  else
    Result := Self;
end;

{ TOperadorExponenciacao }

function TOperadorExponenciacao.calcular(const esquerda,
  direita: TComplexo): TComplexo;
begin
  Result := TComplexo.exp(esquerda, direita);
end;

function TOperadorExponenciacao.clonar(const esquerda,
  direita: TExpressao): TExpressao;
begin
  Result := TOperadorExponenciacao.Create(esquerda, direita);
end;

constructor TOperadorExponenciacao.Create(const esquerda, direita: TExpressao);
begin
  inherited Create('^', esquerda, direita);
end;

function TOperadorExponenciacao.derivar(const esquerda,
  direita: TExpressao): TExpressao;
var
  backup: TExpressao;
begin
  if (esquerda is TNumero) then
    backup := derivarNumero(esquerda, direita)
  else
    backup := derivarPadrao(esquerda, direita);

  Result := TFriend(backup).simplificar;

  if (Result <> backup) then
    backup.Free;
end;

function TOperadorExponenciacao.derivarNumero(const esquerda,
  direita: TExpressao): TExpressao;
var
  numero : TNumero;
  novaEsq,
  novaDir: TExpressao;
begin
  numero  := TNumero(esquerda);
  novaEsq := TOperadorExponenciacao.Create(esquerda, direita);
  Result  := TOperadorMultiplicacao.Create(novaEsq,  TFriend(direita).derivar);

  if (not numero.eConstEuler) then
  begin
    novaDir := TLogaritmoNatural     .Create( TFriend(esquerda).clonar );
    Result  := TOperadorMultiplicacao.Create(Result, novaDir);
  end;
end;

function TOperadorExponenciacao.derivarPadrao(const esquerda,
  direita: TExpressao): TExpressao;
var
  funcao  ,
  novaEsq ,
  novaDir ,
  expoente: TExpressao;
begin
  expoente := TOperadorSubtracao    .Create(direita,  TNumero.Create(UM));
  funcao   := TOperadorExponenciacao.Create(esquerda, expoente);
  novaEsq  := TOperadorMultiplicacao.Create(funcao,   TFriend(direita).clonar);
  novaDir  := TFriend(esquerda)     .derivar;
  Result   := TOperadorMultiplicacao.Create(novaEsq, novaDir);
end;

function TOperadorExponenciacao.simplificar(const esquerda,
  direita: TExpressao): TExpressao;
begin
  if ((direita is TNumero) and TNumero(direita).eUm) then
    Result := TFriend(esquerda).clonar
  else
    Result := Self;
end;

end.
