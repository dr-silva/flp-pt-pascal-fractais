(*******************************************************************************
 *
 * Arquivo  : FractalLotus.Fractais.Expressoes.Sintaxe.pas
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-16
 * Licen�a  : Este arquivo est� sob licen�a Non-Comercial Creative Commons
 * Descri��o: ...
 *
 ******************************************************************************)
unit FractalLotus.Fractais.Expressoes.Sintaxe;

interface
uses
  FractalLotus.Fundacao.Colecoes.PilhaEncadeada,
  FractalLotus.Fractais.Expressoes.Expressao,
  constantes;

type
  TSintaxe = class
  strict private
    frase     : string;
    total     : Integer;
    termos    : TPilhaEncadeada<TExpressao>;
    operadores: TPilhaEncadeada<string>;

    constructor Create(const frase: string);

    function analisar: TExpressao; overload;
    function proximo (const inicio: Integer): Integer;
    function operador(const atual, anterior: Char; const termo: string): Char;

    procedure empilhar   (const termo: string);
    procedure desempilhar(const ate  : string);

    class function comparar  (const esquerda, direita: string): Integer; static; inline;
    class function prioridade(const operador         : string): Integer; static; inline;
  public
    destructor Destroy; override;

    class function analisar(const frase : string    ): TExpressao; overload; static;
    class function derivar (const funcao: TExpressao): TExpressao;           static;
  end;

implementation
uses
  System.RegularExpressions,
  System.SysUtils,
  FractalLotus.Fundacao.Matematica.Complexo,
  terminais,
  operadores_unarios,
  operadores_binarios,
  funcoes;

type
  TFabrica = class
  public
    class function eFuncao  (const funcao  : string): Boolean; static; inline;
    class function eOperador(const operador: Char  ): Boolean; static; inline;
    class function eNumero  (const numero  : string): Boolean; static; inline;
    class function eVariavel(const variavel: string): Boolean; static; inline;

    class function criarOperador(const operador: Char; const esquerda, direita: TExpressao): TExpressao; static;
    class function criarFuncao  (const funcao  : string; const entrada: TExpressao): TExpressao; static;
    class function criarNumero  (const numero  : string): TExpressao; static;
    class function criarVariavel: TExpressao; static;
  end;

  TFriend = class(TExpressao)
  end;

function Matches(const str, pattern: string): Boolean; inline;
begin
  Result := TRegEx.IsMatch(str, pattern, [roIgnoreCase]);
end;

{ TFabrica }

class function TFabrica.criarFuncao(const funcao: string;
  const entrada: TExpressao): TExpressao;
begin
       if (funcao = 'sen')  then Result := TSenoTrigonometrico   .Create(entrada)
  else if (funcao = 'cos')  then Result := TCossenoTrigonometrico.Create(entrada)
  else if (funcao = 'senh') then Result := TSenoHiperbolico      .Create(entrada)
  else if (funcao = 'cosh') then Result := TCossenoHiperbolico   .Create(entrada)
  else if (funcao = 'ln')   then Result := TLogaritmoNatural     .Create(entrada)
  else                           Result := nil;
end;

class function TFabrica.criarNumero(const numero: string): TExpressao;
var
  valor: TComplexo;
begin
  if (numero = 'i') then
    valor := TComplexo.Create(0, 1)
  else if (numero = 'e') then
    valor := TComplexo.Create(Exp(1), 0)
  else if (numero = 'pi') then
    valor := TComplexo.Create(Pi, 0)
  else if (numero = 'ei') then
    valor := TComplexo.Create(0, Exp(1))
  else if (numero = 'pii') then
    valor := TComplexo.Create(0, Pi)
  else if (System.Pos('i', numero) > 0) then
    valor := TComplexo.Create
    (
      0, StrToFloat(Copy(numero, 1, Length(numero) - 1))
    )
  else
    valor := TComplexo.Create(StrToFloat(numero), 0);

  Result := TNumero.Create(valor);
end;

class function TFabrica.criarOperador(const operador: Char; const esquerda,
  direita: TExpressao): TExpressao;
begin
  case (operador) of
    '?': Result := TOperadorPositivo     .Create(direita);
    '!': Result := TOperadorNegativo     .Create(direita);
    '+': Result := TOperadorAdicao       .Create(esquerda, direita);
    '-': Result := TOperadorSubtracao    .Create(esquerda, direita);
    '*': Result := TOperadorMultiplicacao.Create(esquerda, direita);
    '/': Result := TOperadorDivisao      .Create(esquerda, direita);
    '^': Result := TOperadorExponenciacao.Create(esquerda, direita);
    else Result := nil;
  end;
end;

class function TFabrica.criarVariavel: TExpressao;
begin
  Result := TVariavel.Create;
end;

class function TFabrica.eFuncao(const funcao: string): Boolean;
begin
  Result := Matches(funcao, '^((sen|cos)h?|ln)$');
end;

class function TFabrica.eNumero(const numero: string): Boolean;
begin
  Result := Matches(numero, '^((e|pi|\d+(\.\d+)?|\.\d+)i?|i)$');
end;

class function TFabrica.eOperador(const operador: Char): Boolean;
begin
  Result := (operador = '-') or
            (operador = '+') or
            (operador = '*') or
            (operador = '/') or
            (operador = '^') or
            (operador = '(') or
            (operador = ')');
end;

class function TFabrica.eVariavel(const variavel: string): Boolean;
begin
  Result := variavel = 'x';
end;

{ TSintaxe }

function TSintaxe.analisar: TExpressao;
var
  termo   : string;
  operador: Char;
  fim     ,
  inicio  : Integer;
begin
  operador := #0;
  inicio   := 1;

  repeat
    fim   := proximo(inicio);
    termo := Trim( Copy(frase, inicio, fim - inicio) );

    if (termo <> '') then
      empilhar(termo);
    if (fim > total) then
      Break;

    operador := Self.operador(frase[fim], operador, termo);

    if (operador <> '(') then
      desempilhar(operador);
    if (operador <> ')') then
      operadores.empilhar(operador);

    inicio := fim + 1;
  until (inicio > total);

  if ((operadores.total <> 0) or (termos.total <> 1)) then
    Result := nil
  else
    Result := termos.desempilhar;
end;

class function TSintaxe.analisar(const frase: string): TExpressao;
var
  anterior: TExpressao;
begin
  with TSintaxe.Create(string('(' + frase + ')').ToLowerInvariant) do
    try
      try
        Result   := analisar;
        anterior := nil;

        if (Result = nil) then
          raise EArgumentException.Create('frase inv�lida');

        repeat
          if (anterior <> nil) then
            anterior.Free;

          anterior := Result;
          Result   := TFriend(anterior).simplificar;
        until (anterior = Result);
      except
        raise EArgumentException.Create('frase inv�lida');
      end;
    finally
      Free;
    end;
end;

class function TSintaxe.comparar(const esquerda, direita: string): Integer;
begin
  Result := prioridade(esquerda) - prioridade(direita);
end;

constructor TSintaxe.Create(const frase: string);
begin
  Self.frase := frase;
  total      := Length(frase);
  termos     := TPilhaEncadeada<TExpressao>.Create;
  operadores := TPilhaEncadeada<string>    .Create;
end;

class function TSintaxe.derivar(const funcao: TExpressao): TExpressao;
var
  anterior: TExpressao;
begin
  if (funcao = nil) then
    raise EArgumentException.Create('fun��o inv�lida');

  anterior := nil;
  Result   := TFriend(funcao).derivar;

  repeat
    if (anterior <> nil) then
      anterior.Free;

    anterior := Result;
    Result   := TFriend(anterior).simplificar;
  until (anterior = Result);

  if ((Result is TNumero) and (TNumero(Result).eZero or TNumero(Result).eNaN)) then
    raise EArgumentException.Create('fun��o inv�lida');
end;

procedure TSintaxe.desempilhar(const ate: string);
var
  operador: string;
  direita ,
  esquerda: TExpressao;
begin
  while ((not operadores.vazio) and (comparar(ate, operadores.espiar) <= 0)) do
  begin
    operador := operadores.desempilhar;
    direita  := termos    .desempilhar;

    if TFabrica.eFuncao(operador) then
      termos.empilhar( TFabrica.criarFuncao(operador, direita) )
    else if (operador = '(') then
    begin
      termos.empilhar(direita); // devolve o que retirou
      Break;
    end
    else
    begin
      esquerda := nil;

      if TFabrica.eOperador(operador[1]) then
        esquerda := termos.desempilhar;

      termos.empilhar( TFabrica.criarOperador(operador[1], esquerda, direita));
    end;
  end;
end;

destructor TSintaxe.Destroy;
begin
  termos    .Free;
  operadores.Free;

  inherited Destroy;
end;

procedure TSintaxe.empilhar(const termo: string);
begin
  if TFabrica.eFuncao(termo) then
    operadores.empilhar(termo)
  else if TFabrica.eNumero(termo) then
    termos.empilhar( TFabrica.criarNumero(termo) )
  else if TFabrica.eVariavel(termo) then
    termos.empilhar( TFabrica.criarVariavel )
  else
    raise EArgumentOutOfRangeException.Create('');
end;

function TSintaxe.operador(const atual, anterior: Char;
  const termo: string): Char;
begin
  if ((atual <> '-') and (atual <> '+')) then
    Result := atual
  else if ((anterior = '(') and (termo = string.Empty)) then
  begin
    if (atual = '-') then
      Result := '!'
    else
      Result := '?';
  end
  else
    Result := atual;
end;

class function TSintaxe.prioridade(const operador: string): Integer;
begin
  if TFabrica.eFuncao(operador) then
    Result := 3
  else
    case (operador[1]) of
      '+', '-': Result := 1;
      '*', '/': Result := 2;
      '^'     : Result := 3;
      '?', '!': Result := 4;
      else      Result := 0;
    end;
end;

function TSintaxe.proximo(const inicio: Integer): Integer;
var
  i: Integer;
begin
  Result := total;

  for i := inicio to total - 1 do
    if TFabrica.eOperador(frase[i]) then
    begin
      Result := i;
      Break;
    end;
end;

end.
