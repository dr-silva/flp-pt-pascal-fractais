unit terminais;

interface
uses
  FractalLotus.Fundacao.Matematica.Complexo,
  FractalLotus.Fractais.Expressoes.Expressao,
  constantes;

type
  TVariavel = class (TExpressao)
  protected
    function clonar     : TExpressao; override;
    function derivar    : TExpressao; override;
    function simplificar: TExpressao; override;
  public
    function ToString: string; override;

    function valorar(const x: TComplexo): TComplexo; override;
  end;

  TNumero = class (TExpressao)
  strict private
    fValor: TComplexo;
  protected
    function clonar     : TExpressao; override;
    function derivar    : TExpressao; override;
    function simplificar: TExpressao; override;
  public
    constructor Create(const valor: TComplexo);

    function ToString: string; override;

    function valorar(const x: TComplexo): TComplexo; override;

    function eZero      : Boolean; inline;
    function eNaN       : Boolean; inline;
    function eUm        : Boolean; inline;
    function eConstEuler: Boolean; inline;
    function eReal      : Boolean; inline;
    function eImaginario: Boolean; inline;

    property valor: TComplexo read fValor;
  end;

const
  UM: TComplexo = (re: 1; im: 0);

implementation

{ TVariavel }

function TVariavel.clonar: TExpressao;
begin
  Result := TVariavel.Create;
end;

function TVariavel.derivar: TExpressao;
begin
  Result := TNumero.Create(UM);
end;

function TVariavel.simplificar: TExpressao;
begin
  Result := Self;
end;

function TVariavel.ToString: string;
begin
  Result := 'x';
end;

function TVariavel.valorar(const x: TComplexo): TComplexo;
begin
  Result := x;
end;

{ TNumero }

function TNumero.clonar: TExpressao;
begin
  Result := TNumero.Create(fValor);
end;

constructor TNumero.Create(const valor: TComplexo);
begin
  fValor := valor;
end;

function TNumero.derivar: TExpressao;
begin
  Result := TNumero.Create(TComplexo.ZERO);
end;

function TNumero.eConstEuler: Boolean;
begin
  Result := TConstante.saoIguais(fValor, TComplexo.Create(Exp(1), 0));
end;

function TNumero.eImaginario: Boolean;
begin
  Result := TConstante.eImaginario(fValor);
end;

function TNumero.eNaN: Boolean;
begin
  Result := TComplexo.eNaN(fValor);
end;

function TNumero.eReal: Boolean;
begin
  Result := TConstante.eReal(fValor);
end;

function TNumero.eUm: Boolean;
begin
  Result := TConstante.saoIguais(fValor, UM);
end;

function TNumero.eZero: Boolean;
begin
  Result := TConstante.eZero(fValor);
end;

function TNumero.simplificar: TExpressao;
begin
  Result := Self;
end;

function TNumero.ToString: string;
begin
  Result := '(' + fValor.toString + ')';
end;

function TNumero.valorar(const x: TComplexo): TComplexo;
begin
  Result := fValor;
end;

end.
