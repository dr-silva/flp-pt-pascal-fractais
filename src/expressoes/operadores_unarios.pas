unit operadores_unarios;

interface
uses
  FractalLotus.Fundacao.Matematica.Complexo,
  FractalLotus.Fractais.Expressoes.Expressao;

type
  TOperadorUnario = class abstract (TExpressao)
  strict private
    fOperador: Char;
    fOperando: TExpressao;
  protected
    function clonar     : TExpressao; overload; override;
    function derivar    : TExpressao;           override;
    function simplificar: TExpressao; overload; override;
  strict protected
    function calcular   (const operando: TComplexo ): TComplexo;                         virtual; abstract;
    function clonar     (const operando: TExpressao): TExpressao; reintroduce; overload; virtual; abstract;
    function simplificar(const operando: TExpressao): TExpressao; reintroduce; overload; virtual; abstract;
  public
    constructor Create(const operador: Char; const operando: TExpressao);
    destructor  Destroy; override;

    function ToString: string; override;

    function valorar(const x: TComplexo): TComplexo; override;
  end;

  TOperadorPositivo = class (TOperadorUnario)
  strict protected
    function calcular   (const operando: TComplexo ): TComplexo;  override;
    function clonar     (const operando: TExpressao): TExpressao; override;
    function simplificar(const operando: TExpressao): TExpressao; override;
  public
    constructor Create(const operando: TExpressao);
  end;

  TOperadorNegativo = class (TOperadorUnario)
  strict protected
    function calcular   (const operando: TComplexo ): TComplexo;  override;
    function clonar     (const operando: TExpressao): TExpressao; override;
    function simplificar(const operando: TExpressao): TExpressao; override;
  public
    constructor Create(const operando: TExpressao);
  end;

implementation
uses
  terminais;

type
  TFriend = class(TExpressao)
  end;

{ TOperadorUnario }

function TOperadorUnario.clonar: TExpressao;
begin
  Result := clonar(TFriend(fOperando).clonar);
end;

constructor TOperadorUnario.Create(const operador: Char;
  const operando: TExpressao);
begin
  fOperador := operador;
  fOperando := operando;
end;

function TOperadorUnario.derivar: TExpressao;
begin
  Result := clonar(TFriend(fOperando).derivar);
end;

destructor TOperadorUnario.Destroy;
begin
  fOperando.Free;

  inherited Destroy;
end;

function TOperadorUnario.simplificar: TExpressao;
var
  backup: TExpressao;
begin
  backup    := fOperando;
  fOperando := TFriend(fOperando).simplificar;

  if (backup <> fOperando) then
    backup.Free;

  if (fOperando is TNumero) then
    Result := TNumero.Create(calcular(TNumero(fOperando).valor))
  else if (fOperando.ClassType = Self.ClassType) then
    Result := TOperadorUnario(fOperando).fOperando
  else
    Result := simplificar(fOperando);
end;

function TOperadorUnario.valorar(const x: TComplexo): TComplexo;
begin
  Result := calcular(fOperando.valorar(x));
end;

function TOperadorUnario.ToString: string;
begin
  Result := '(' + fOperador + fOperando.ToString + ')';
end;

{ TOperadorPositivo }

function TOperadorPositivo.calcular(const operando: TComplexo): TComplexo;
begin
  Result := +operando;
end;

function TOperadorPositivo.clonar(const operando: TExpressao): TExpressao;
begin
  Result := TOperadorPositivo.Create(operando);
end;

constructor TOperadorPositivo.Create(const operando: TExpressao);
begin
  inherited Create('+', operando);
end;

function TOperadorPositivo.simplificar(const operando: TExpressao): TExpressao;
begin
  Result := operando;
end;

{ TOperadorNegativo }

function TOperadorNegativo.calcular(const operando: TComplexo): TComplexo;
begin
  Result := -operando;
end;

function TOperadorNegativo.clonar(const operando: TExpressao): TExpressao;
begin
  Result := TOperadorNegativo.Create(operando);
end;

constructor TOperadorNegativo.Create(const operando: TExpressao);
begin
  inherited Create('-', operando);
end;

function TOperadorNegativo.simplificar(const operando: TExpressao): TExpressao;
begin
  Result := Self;
end;

end.
