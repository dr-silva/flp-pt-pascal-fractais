unit FractalLotus.Fractais.Expressoes.Expressao;

interface
uses
  FractalLotus.Fundacao.Matematica.Complexo;

type
  TExpressao = class abstract
  protected
    function clonar     : TExpressao; virtual; abstract;
    function derivar    : TExpressao; virtual; abstract;
    function simplificar: TExpressao; virtual; abstract;
  public
    function valorar(const x: TComplexo): TComplexo; virtual; abstract;
  end;

implementation

end.
