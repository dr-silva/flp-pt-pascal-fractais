unit funcoes;

interface
uses
  FractalLotus.Fundacao.Matematica.Complexo,
  FractalLotus.Fractais.Expressoes.Expressao;

type
  TFuncao = class abstract (TExpressao)
  strict private
    fNome   : string;
    fEntrada: TExpressao;
  protected
    function clonar     : TExpressao; overload; override;
    function derivar    : TExpressao; overload; override;
    function simplificar: TExpressao;           override;
  strict protected
    function calcular(const entrada: TComplexo ): TComplexo;                         virtual; abstract;
    function derivar (const entrada: TExpressao): TExpressao; reintroduce; overload; virtual; abstract;
    function clonar  (const entrada: TExpressao): TExpressao; reintroduce; overload; virtual; abstract;
  public
    constructor Create(const nome: string; const entrada: TExpressao);
    destructor  Destroy; override;

    function ToString: string; override;

    function valorar(const x: TComplexo): TComplexo; override;
  end;

  TSenoTrigonometrico = class (TFuncao)
  strict protected
    function calcular(const entrada: TComplexo ): TComplexo;  override;
    function derivar (const entrada: TExpressao): TExpressao; override;
    function clonar  (const entrada: TExpressao): TExpressao; override;
  public
    constructor Create(const entrada: TExpressao);
  end;

  TCossenoTrigonometrico = class (TFuncao)
  strict protected
    function calcular(const entrada: TComplexo ): TComplexo;  override;
    function derivar (const entrada: TExpressao): TExpressao; override;
    function clonar  (const entrada: TExpressao): TExpressao; override;
  public
    constructor Create(const entrada: TExpressao);
  end;

  TSenoHiperbolico = class (TFuncao)
  strict protected
    function calcular(const entrada: TComplexo ): TComplexo;  override;
    function derivar (const entrada: TExpressao): TExpressao; override;
    function clonar  (const entrada: TExpressao): TExpressao; override;
  public
    constructor Create(const entrada: TExpressao);
  end;

  TCossenoHiperbolico = class (TFuncao)
  strict protected
    function calcular(const entrada: TComplexo ): TComplexo;  override;
    function derivar (const entrada: TExpressao): TExpressao; override;
    function clonar  (const entrada: TExpressao): TExpressao; override;
  public
    constructor Create(const entrada: TExpressao);
  end;

  TLogaritmoNatural = class (TFuncao)
  strict protected
    function calcular(const entrada: TComplexo ): TComplexo;  override;
    function derivar (const entrada: TExpressao): TExpressao; override;
    function clonar  (const entrada: TExpressao): TExpressao; override;
  public
    constructor Create(const entrada: TExpressao);
  end;

implementation
uses
  terminais,
  operadores_unarios,
  operadores_binarios;

type
  TFriend = class(TExpressao)
  end;

{ TFuncao }

function TFuncao.clonar: TExpressao;
begin
  Result := clonar( TFriend(fEntrada).clonar );
end;

constructor TFuncao.Create(const nome: string; const entrada: TExpressao);
begin
  fNome    := nome;
  fEntrada := entrada;
end;

function TFuncao.derivar: TExpressao;
var
  backup  ,
  derivada: TExpressao;
begin
  derivada := derivar( TFriend(fEntrada).clonar );

  if (fEntrada is TVariavel) then
    backup := derivada
  else
    backup := TOperadorMultiplicacao.Create(derivada, TFriend(fEntrada).derivar);

  Result := TFriend(backup).simplificar;

  if (Result <> backup) then
    backup.Free;
end;

destructor TFuncao.Destroy;
begin
  fEntrada.Free;

  inherited Destroy;
end;

function TFuncao.simplificar: TExpressao;
var
  backup: TExpressao;
begin
  backup   := fEntrada;
  fEntrada := TFriend(fEntrada).simplificar;

  if (fEntrada <> backup) then
    backup.Free;

  if (fEntrada is TNumero) then
    Result := TNumero.Create(calcular(TNumero(fEntrada).valor))
  else
    Result := Self;
end;

function TFuncao.ToString: string;
begin
  Result := fNome + '(' + fEntrada.ToString + ')';
end;

function TFuncao.valorar(const x: TComplexo): TComplexo;
begin
  Result := calcular( fEntrada.valorar(x) );
end;

{ TSenoTrigonometrico }

function TSenoTrigonometrico.calcular(const entrada: TComplexo): TComplexo;
begin
  Result := TComplexo.sen(entrada);
end;

function TSenoTrigonometrico.clonar(const entrada: TExpressao): TExpressao;
begin
  Result := TSenoTrigonometrico.Create(entrada);
end;

constructor TSenoTrigonometrico.Create(const entrada: TExpressao);
begin
  inherited Create('sen', entrada);
end;

function TSenoTrigonometrico.derivar(const entrada: TExpressao): TExpressao;
begin
  Result := TCossenoTrigonometrico.Create(entrada);
end;

{ TCossenoTrigonometrico }

function TCossenoTrigonometrico.calcular(const entrada: TComplexo): TComplexo;
begin
  Result := TComplexo.cos(entrada);
end;

function TCossenoTrigonometrico.clonar(const entrada: TExpressao): TExpressao;
begin
  Result := TCossenoTrigonometrico.Create(entrada);
end;

constructor TCossenoTrigonometrico.Create(const entrada: TExpressao);
begin
  inherited Create('cos', entrada);
end;

function TCossenoTrigonometrico.derivar(const entrada: TExpressao): TExpressao;
begin
  Result := TOperadorNegativo.Create(TSenoTrigonometrico.Create(entrada));
end;

{ TSenoHiperbolico }

function TSenoHiperbolico.calcular(const entrada: TComplexo): TComplexo;
begin
  Result := TComplexo.senh(entrada);
end;

function TSenoHiperbolico.clonar(const entrada: TExpressao): TExpressao;
begin
  Result := TSenoHiperbolico.Create(entrada);
end;

constructor TSenoHiperbolico.Create(const entrada: TExpressao);
begin
  inherited Create('senh', entrada);
end;

function TSenoHiperbolico.derivar(const entrada: TExpressao): TExpressao;
begin
  Result := TCossenoHiperbolico.Create(entrada);
end;

{ TCossenoHiperbolico }

function TCossenoHiperbolico.calcular(const entrada: TComplexo): TComplexo;
begin
  Result := TComplexo.cosh(entrada);
end;

function TCossenoHiperbolico.clonar(const entrada: TExpressao): TExpressao;
begin
  Result := TCossenoHiperbolico.Create(entrada);
end;

constructor TCossenoHiperbolico.Create(const entrada: TExpressao);
begin
  inherited Create('cosh', entrada);
end;

function TCossenoHiperbolico.derivar(const entrada: TExpressao): TExpressao;
begin
  Result := TSenoHiperbolico.Create(entrada);
end;

{ TLogaritmoNatural }

function TLogaritmoNatural.calcular(const entrada: TComplexo): TComplexo;
begin
  Result := TComplexo.ln(entrada);
end;

function TLogaritmoNatural.clonar(const entrada: TExpressao): TExpressao;
begin
  Result := TLogaritmoNatural.Create(entrada);
end;

constructor TLogaritmoNatural.Create(const entrada: TExpressao);
begin
  inherited Create('ln', entrada);
end;

function TLogaritmoNatural.derivar(const entrada: TExpressao): TExpressao;
begin
  Result := TOperadorDivisao.Create(TNumero.Create(UM), entrada);
end;

end.
