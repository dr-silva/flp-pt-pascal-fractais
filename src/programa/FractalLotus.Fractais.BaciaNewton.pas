unit FractalLotus.Fractais.BaciaNewton;

interface
uses
  FractalLotus.Fundacao.Cerne.Estruturas,
  FractalLotus.Fundacao.Colecoes.BolsaEncadeada,
  FractalLotus.Fundacao.Matematica.Complexo,
  FractalLotus.Fundacao.Matematica.GeradorCongruencial,
  FractalLotus.Fundacao.EntradasSaidas.ImagemBitmap,
  FractalLotus.Fractais.Config,
  FractalLotus.Fractais.FuncaoHolomorfa;

type
  TBaciaNewton = class
  strict private type
    TParRaizCor = class
    strict private
      fCor : TAlphaColor;
      fRaiz: TComplexo;
    public
      constructor Create(const raiz: TComplexo; const cor: TAlphaColor);

      property cor : TAlphaColor read fCor;
      property raiz: TComplexo   read fRaiz;
    end;
  strict private
    tempo    : TPeriodo;
    funcao   : TFuncaoHolomorfa;
    raizes   : TBolsaEncadeada<TParRaizCor>;
    inicio   ,
    dimensao : TComplexo;
    arquivo  : string;
    fractal  : TImagemBitmap;
    aleatorio: IGeradorCongruencial;

    function obterCor(const raiz: TComplexo; const iteracoes: Integer): TAlphaColor;
    function calcularPonto(const linha, coluna: Integer): TComplexo;
  public
    constructor Create(const config: TConfig);
    destructor  Destroy; override;

    procedure desenhar;
    procedure salvar;
  end;

implementation
uses
  System.UITypes,
  System.SysUtils,
  FractalLotus.Fundacao.EntradasSaidas.Console,
  FractalLotus.Fundacao.Matematica.FabricaGeradorCongruencial,
  constantes;

{ TBaciaNewton.TParRaizCor }

constructor TBaciaNewton.TParRaizCor.Create(const raiz: TComplexo;
  const cor: TAlphaColor);
begin
  fCor  := cor;
  fRaiz := raiz;
end;

{ TBaciaNewton }

function TBaciaNewton.calcularPonto(const linha, coluna: Integer): TComplexo;
begin
  Result.re := inicio.re + dimensao.re * (coluna / (fractal.largura - 1));
  Result.im := inicio.im + dimensao.im * (linha  / (fractal.altura  - 1));
end;

constructor TBaciaNewton.Create(const config: TConfig);
begin
  arquivo   := config.arquivo;
  inicio    := TComplexo                   .Create(config.reInicial,  config.imInicial);
  dimensao  := TComplexo                   .Create(config.reDimensao, config.imDimensao);
  fractal   := TImagemBitmap               .Create(config.imgAltura,  config.imgLargura);
  funcao    := TFuncaoHolomorfa            .Create(config.funcao,     config.derivada);
  raizes    := TBolsaEncadeada<TParRaizCor>.Create;
  aleatorio := TFabricaGeradorCongruencial .criar
  (
    'posix', TTipoGeradorCongruencial.tgcLinear, config.semente
  );

  fractal.originarDoInferiorEsquerdo;
end;

procedure TBaciaNewton.desenhar;
var
  valor    : TComplexo;
  linha    ,
  coluna   ,
  iteracoes: Integer;
begin
  with TTemporizador.Create do
    try
      for linha := 0 to fractal.altura  - 1 do
        for coluna := 0 to fractal.largura - 1 do
        begin
          valor := calcularPonto(linha, coluna);
          valor := funcao.obterRaiz(valor, iteracoes);

          fractal[linha, coluna] := obterCor(valor, iteracoes);
        end;

      tempo := decorrer;
    finally
      Free;
    end;
end;

destructor TBaciaNewton.Destroy;
var
  item: TParRaizCor;
begin
  for item in raizes do
    item.Free;

  aleatorio := nil;

  funcao .Free;
  raizes .Free;
  fractal.Free;

  inherited Destroy;
end;

function TBaciaNewton.obterCor(const raiz: TComplexo;
  const iteracoes: Integer): TAlphaColor;
var
  item       : TParRaizCor;
  pResult    : PAlphaColorRec;
  porcentagem: Double;
begin
  if (TComplexo.eNaN(raiz) or TConstante.eTeto(iteracoes)) then
    Exit(TAlphaColorRec.Black);

  Result  := 0;
  pResult := @Result;

  for item in raizes do
    if TConstante.saoIguais(raiz, item.raiz) then
    begin
      Result := item.cor;
      Break;
    end;

  if (Result = 0) then
  begin
    Result := TAlphaColorRec.Alpha or Cardinal(aleatorio.uniforme($FFFFFF));

    raizes.guardar(TParRaizCor.Create(raiz, Result));
  end;

  porcentagem := TConstante.porcentagem(iteracoes);
  pResult^.R  := Byte(Trunc(pResult^.R * porcentagem));
  pResult^.G  := Byte(Trunc(pResult^.G * porcentagem));
  pResult^.B  := Byte(Trunc(pResult^.B * porcentagem));
end;

procedure TBaciaNewton.salvar;
begin
  fractal.salvar(arquivo);

  // imprime os dados
  funcao.imprimirInfos;
  TConsole.escreverLinha('semente : %d', [aleatorio.semente]);
  TConsole.escreverLinha('arquivo : %s', [arquivo]);
  TConsole.escreverLinha('tempo   : %s', [tempo.toString]);
end;

end.
