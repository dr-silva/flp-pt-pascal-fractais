unit constantes;

interface
uses
  FractalLotus.Fundacao.Matematica.Complexo;

type
  TConstante = class
  private const
    EPSILON  : Double = 1e-9;
    ITERACOES: Integer = 40;
  public
    class function eTeto      (const valor: Integer): Boolean; static; inline;
    class function porcentagem(const valor: Integer): Double;  static; inline;

    class function eZero      (const valor: TComplexo): Boolean; static; inline;
    class function eReal      (const valor: TComplexo): Boolean; static; inline;
    class function eImaginario(const valor: TComplexo): Boolean; static; inline;
    class function saoIguais  (const esquerda, direita: TComplexo): Boolean; static; inline;
  end;

implementation

{ TConstante }

class function TConstante.eImaginario(const valor: TComplexo): Boolean;
begin
  Result := Abs(valor.re) < EPSILON;
end;

class function TConstante.eReal(const valor: TComplexo): Boolean;
begin
  Result := Abs(valor.im) < EPSILON;
end;

class function TConstante.eTeto(const valor: Integer): Boolean;
begin
  Result := valor > ITERACOES;
end;

class function TConstante.eZero(const valor: TComplexo): Boolean;
begin
  Result := TComplexo.eZero(valor, EPSILON);
end;

class function TConstante.porcentagem(const valor: Integer): Double;
var
  minimo: Integer;
begin
  if eTeto(valor) then
    minimo := ITERACOES
  else
    minimo := valor;

  Result := 1 - (minimo / ITERACOES);
end;

class function TConstante.saoIguais(const esquerda,
  direita: TComplexo): Boolean;
begin
  Result := (esquerda - direita).modulo < EPSILON;
end;

end.
