unit FractalLotus.Fractais.FuncaoHolomorfa;

interface
uses
  FractalLotus.Fundacao.Matematica.Complexo,
  FractalLotus.Fractais.Expressoes.Expressao;

type
  TFuncaoHolomorfa = class
  strict private
    funcao  ,
    derivada: TExpressao;
  public
    constructor Create(const funcao, derivada: TExpressao);

    function obterRaiz(const x: TComplexo; var iteracoes: Integer): TComplexo;

    procedure imprimirInfos;
  end;

implementation
uses
  FractalLotus.Fundacao.EntradasSaidas.Console,
  constantes;

{ TFuncaoHolomorfa }

constructor TFuncaoHolomorfa.Create(const funcao, derivada: TExpressao);
begin
  Self.funcao   := funcao;
  Self.derivada := derivada;
end;

procedure TFuncaoHolomorfa.imprimirInfos;
begin
  TConsole.escreverLinha('funcao  : %s', [funcao  .ToString]);
  TConsole.escreverLinha('derivada: %s', [derivada.ToString]);
end;

function TFuncaoHolomorfa.obterRaiz(const x: TComplexo;
  var iteracoes: Integer): TComplexo;
var
  f, d, atual, anterior: TComplexo;
begin
  atual     := x;
  Result    := TComplexo.NaN;
  iteracoes := 0;

  repeat
    if TConstante.eTeto(iteracoes) then
      Exit;

    anterior := atual;
    f        := funcao  .valorar(anterior);
    d        := derivada.valorar(anterior);

    if (TComplexo.eNaN(f) or TComplexo.eNaN(d) or TConstante.eZero(d)) then
      Exit;

    atual     := anterior - f / d;
    iteracoes := iteracoes + 1;
  until TConstante.saoIguais(atual, anterior);

  if not TConstante.eTeto(iteracoes) then
    Result := atual;
end;

end.
