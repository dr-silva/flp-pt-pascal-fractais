unit FractalLotus.Fractais.Config;

interface
uses
  FractalLotus.Fundacao.Colecoes.BolsaEncadeada,
  FractalLotus.Fundacao.Matematica.Complexo,
  FractalLotus.Fractais.Expressoes.Expressao;

type
  TConfig = class
  strict private
    fErros     : TBolsaEncadeada<string>;
    fReInicial ,
    fImInicial ,
    fReDimensao,
    fImDimensao: Double;
    fImgAltura ,
    fImgLargura: Integer;
    fSemente   : Cardinal;
    fArquivo   : string;
    fFuncao    ,
    fDerivada  : TExpressao;

    function lerHaErros: Boolean;

    procedure lerDado      (const str         : string; const linha: Integer);
    procedure lerChaveValor(const chave, valor: string; const linha: Integer);
    procedure lerReInicial (const valor       : string; const linha: Integer);
    procedure lerImInicial (const valor       : string; const linha: Integer);
    procedure lerReDimensao(const valor       : string; const linha: Integer);
    procedure lerImDimensao(const valor       : string; const linha: Integer);
    procedure lerImgAltura (const valor       : string; const linha: Integer);
    procedure lerImgLargura(const valor       : string; const linha: Integer);
    procedure lerFuncao    (const valor       : string; const linha: Integer);
    procedure lerDerivada  (const valor       : string; const linha: Integer); overload;
    procedure lerSemente   (const valor       : string; const linha: Integer);
    procedure lerArquivo   (const valor       : string; const linha: Integer);
    procedure lerDerivada; overload;
    procedure validarLeitura;
  public
    constructor Create;
    destructor Destroy; override;

    procedure lerDados;
    procedure imprimirErros;

    class procedure imprimirArquivo; static;

    property haErros   : Boolean    read lerHaErros;
    property reInicial : Double     read fReInicial;
    property imInicial : Double     read fImInicial;
    property reDimensao: Double     read fReDimensao;
    property imDimensao: Double     read fImDimensao;
    property imgAltura : Integer    read fImgAltura;
    property imgLargura: Integer    read fImgLargura;
    property semente   : Cardinal   read fSemente;
    property arquivo   : string     read fArquivo;
    property funcao    : TExpressao read fFuncao;
    property derivada  : TExpressao read fDerivada;
  end;

implementation
uses
  System.Classes,
  System.IOUtils,
  System.SysUtils,
  Winapi.Windows,
  FractalLotus.Fundacao.EntradasSaidas.Console,
  FractalLotus.Fundacao.EntradasSaidas.ImagemBitmap,
  FractalLotus.Fractais.Expressoes.Sintaxe;

{ TConfig }

constructor TConfig.Create;
begin
  fErros      := TBolsaEncadeada<string>.Create;
  fReInicial  := Double.NaN;
  fImInicial  := Double.NaN;
  fReDimensao := Double.NaN;
  fImDimensao := Double.NaN;
  fImgAltura  := Integer.MinValue;
  fImgLargura := Integer.MinValue;
  fSemente    := TThread.GetTickCount;
  fArquivo    := 'NULL';
  fFuncao     := nil;
  fDerivada   := nil;
end;

destructor TConfig.Destroy;
begin
  if (fFuncao <> nil) then
    fFuncao.Free;
  if (fDerivada <> nil) then
    fDerivada.Free;

  fErros.Free;

  inherited Destroy;
end;

class procedure TConfig.imprimirArquivo;
begin
  TConsole.escreverLinha('reInicial =0         # deve ser um ponto-flutuante');
  TConsole.escreverLinha('imInicial =0         # deve ser um ponto-flutuante');
  TConsole.escreverLinha('reDimensao=1         # deve ser um ponto-flutuante positivo');
  TConsole.escreverLinha('imDimensao=1         # deve ser um ponto-flutuante positivo');
  TConsole.escreverLinha('imgAltura =100       # deve ser um numero inteiro positivo');
  TConsole.escreverLinha('imgLargura=100       # deve ser um numero inteiro positivo');
  TConsole.escreverLinha('funcao    =x^4-1     # deve ser uma expressao que represente a funcao');
  TConsole.escreverLinha('#derivada  =4*x^3     # deve ser uma expressao que represente a derivada');
  TConsole.escreverLinha('#semente   =20211020  # deve ser um numero inteiro positivo');
  TConsole.escreverLinha('arquivo   =teste.png # deve ser arquivo cuja extensao e PNG');
end;

procedure TConfig.imprimirErros;
var
  erro: string;
begin
  for erro in fErros do
    TConsole.escreverLinha(erro);
end;

procedure TConfig.lerArquivo(const valor: string; const linha: Integer);
begin
  try
    fArquivo := string.Empty;

    with TImagemBitmap.Create(1, 1) do
      try
        salvar(valor);
      finally
        Free;
      end;

    fArquivo := valor;
  except
    fErros.guardar(Format('O valor da chave "arquivo" (na linha %d) n�o � um arquivo v�lido.', [linha]));
  end;
end;

procedure TConfig.lerChaveValor(const chave, valor: string;
  const linha: Integer);
begin
       if (chave = 'reinicial')  then lerReInicial (valor, linha)
  else if (chave = 'iminicial')  then lerImInicial (valor, linha)
  else if (chave = 'redimensao') then lerReDimensao(valor, linha)
  else if (chave = 'imdimensao') then lerImDimensao(valor, linha)
  else if (chave = 'imgaltura')  then lerImgAltura (valor, linha)
  else if (chave = 'imglargura') then lerImgLargura(valor, linha)
  else if (chave = 'funcao')     then lerFuncao    (valor, linha)
  else if (chave = 'derivada')   then lerDerivada  (valor, linha)
  else if (chave = 'semente')    then lerSemente   (valor, linha)
  else if (chave = 'arquivo')    then lerArquivo   (valor, linha)
  else
    fErros.guardar(Format('A chave "%s" na linha %d n�o � v�lida', [chave, linha]));
end;

procedure TConfig.lerDado(const str: string; const linha: Integer);
var
  i: Integer;
  s: string;
  par: TArray<string>;
begin
  i := str.IndexOf('#');
  if (i >= 0) then
    s := str.Substring(0, i).Trim
  else
    s := str.Trim;

  if s.IsEmpty then
    Exit;

  par := s.Split(['=']);

  if (Length(par) <> 2) then
    fErros.guardar(string.Format('A linha %d est� fora do padr�o chave=valor', [linha]))
  else
    lerChaveValor(par[0].Trim.ToLowerInvariant, par[1].Trim.ToLowerInvariant, linha);
end;

procedure TConfig.lerDados;
var
  input : TFileStream;
  leitor: TStreamReader;
  linha : Integer;
begin
  input  := TFileStream  .Create( GetStdHandle(STD_INPUT_HANDLE) );
  //input := TFileStream.Create('config.txt', fmOpenRead);
  leitor := TStreamReader.Create(input);
  try
    if (input.Size <= 0) then
    begin
      fErros.guardar('A entrada do console n�o foi redirecionada.');
      fErros.guardar('Use a op��o "ajuda" para mais informa��es.');
    end
    else
    begin
      linha := 1;

      while not leitor.EndOfStream do
      begin
        lerDado(leitor.ReadLine, linha);

        linha := linha + 1;
      end;

      lerDerivada;
      validarLeitura;
    end;
  finally
    leitor.Close;

    leitor.Free;
    input .Free;
  end;
end;

procedure TConfig.lerDerivada;
begin
  if ((fFuncao <> nil) and (fDerivada = nil)) then
    try
      fDerivada := TSintaxe.derivar(fFuncao);
    except
      fErros.guardar(Format('A fun��o "%s" n�o � uma fun��o holomorfa.', [fFuncao.ToString]));
    end;
end;

procedure TConfig.lerDerivada(const valor: string; const linha: Integer);
begin
  try
    fDerivada := TSintaxe.analisar(valor);
  except
    fErros.guardar(Format('O valor da chave "derivada" (na linha %d) n�o � uma express�o v�lida.', [linha]));
  end;
end;

procedure TConfig.lerFuncao(const valor: string; const linha: Integer);
begin
  try
    fFuncao := TSintaxe.analisar(valor);
  except
    fErros.guardar(Format('O valor da chave "funcao" (na linha %d) n�o � uma express�o v�lida.', [linha]));
  end;
end;

function TConfig.lerHaErros: Boolean;
begin
  Result := not fErros.vazio;
end;

procedure TConfig.lerImDimensao(const valor: string; const linha: Integer);
var
  temp: Double;
begin
  if not Double.TryParse(valor, temp) then
    fErros.guardar(Format('O valor da chave "imDimensao" (na linha %d) n�o � um n�mero v�lido.', [linha]))
  else if (temp > 0) then
    fImDimensao := temp
  else
    fErros.guardar(Format('O valor da chave "imDimensao" (na linha %d) n�o � um n�mero positivo v�lido.', [linha]))
end;

procedure TConfig.lerImgAltura(const valor: string; const linha: Integer);
var
  temp: Integer;
begin
  if not Integer.TryParse(valor, temp) then
    fErros.guardar(Format('O valor da chave "imgAltura" (na linha %d) n�o � um n�mero v�lido.', [linha]))
  else if (temp > 0) then
    fImgAltura := temp
  else
    fErros.guardar(Format('O valor da chave "imgAltura" (na linha %d) n�o � um n�mero positivo v�lido.', [linha]))
end;

procedure TConfig.lerImgLargura(const valor: string; const linha: Integer);
var
  temp: Integer;
begin
  if not Integer.TryParse(valor, temp) then
    fErros.guardar(Format('O valor da chave "imgLargura" (na linha %d) n�o � um n�mero v�lido.', [linha]))
  else if (temp > 0) then
    fImgLargura := temp
  else
    fErros.guardar(Format('O valor da chave "imgLargura" (na linha %d) n�o � um n�mero positivo v�lido.', [linha]))
end;

procedure TConfig.lerImInicial(const valor: string; const linha: Integer);
var
  temp: Double;
begin
  if Double.TryParse(valor, temp) then
    fImInicial := temp
  else
    fErros.guardar(Format('O valor da chave "imInicial" (na linha %d) n�o � um n�mero v�lido.', [linha]));
end;

procedure TConfig.lerReDimensao(const valor: string; const linha: Integer);
var
  temp: Double;
begin
  if not Double.TryParse(valor, temp) then
    fErros.guardar(Format('O valor da chave "reDimensao" (na linha %d) n�o � um n�mero v�lido.', [linha]))
  else if (temp > 0) then
    fReDimensao := temp
  else
    fErros.guardar(Format('O valor da chave "reDimensao" (na linha %d) n�o � um n�mero positivo v�lido.', [linha]))
end;

procedure TConfig.lerReInicial(const valor: string; const linha: Integer);
var
  temp: Double;
begin
  if Double.TryParse(valor, temp) then
    fReInicial := temp
  else
    fErros.guardar(Format('O valor da chave "reInicial" (na linha %d) n�o � um n�mero v�lido.', [linha]));
end;

procedure TConfig.lerSemente(const valor: string; const linha: Integer);
var
  temp: Cardinal;
begin
  if Cardinal.TryParse(valor, temp) then
    fSemente := temp
  else
    fErros.guardar(Format('O valor da chave "semente" (na linha %d) n�o � um n�mero positivo v�lido.', [linha]))
end;

procedure TConfig.validarLeitura;
begin
  if (Double.IsNan(fReInicial)) then
    fErros.guardar('A chave "reInicial" n�o consta no arquivo de configura��es');
  if (Double.IsNan(fImInicial)) then
    fErros.guardar('A chave "imInicial" n�o consta no arquivo de configura��es');
  if (Double.IsNan(fReDimensao)) then
    fErros.guardar('A chave "reDimensao" n�o consta no arquivo de configura��es');
  if (Double.IsNan(fImDimensao)) then
    fErros.guardar('A chave "imDimensao" n�o consta no arquivo de configura��es');
  if (fImgAltura = Integer.MinValue) then
    fErros.guardar('A chave "imgAltura" n�o consta no arquivo de configura��es');
  if (fImgLargura = Integer.MinValue) then
    fErros.guardar('A chave "imgLargura" n�o consta no arquivo de configura��es');
  if (fArquivo = 'NULL') then
    fErros.guardar('A chave "arquivo" n�o consta no arquivo de configura��es');
  if (fFuncao = nil) then
    fErros.guardar('A chave "funcao" n�o consta no arquivo de configura��es');
end;

end.
