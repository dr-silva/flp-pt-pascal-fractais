unit execucoes;

interface

procedure executar;                    overload;
procedure executar(const arg: string); overload;
procedure imprimirAjuda;
procedure imprimirErro;

implementation
uses
  System.SysUtils,
  FractalLotus.Fundacao.EntradasSaidas.Console,
  FractalLotus.Fractais.Config,
  FractalLotus.Fractais.BaciaNewton;

procedure executar;
var
  config: TConfig;
begin
  config := TConfig.Create;
  try
    TConsole.escreverLinha('Lendo os dados...');
    config.lerDados;

    if config.haErros then
      config.imprimirErros
    else
      with TBaciaNewton.Create(config) do
        try
          TConsole.escreverLinha('Preparando o fractal...');
          desenhar;
          salvar;
        finally
          Free;
        end;
  finally
    config.Free;
  end;
end;

procedure executar(const arg: string);
var
  neo: string;
begin
  neo := arg.ToLowerInvariant;

  if (neo = 'config') then
    TConfig.imprimirArquivo
  else if (neo = 'ajuda') then
    imprimirAjuda
  else
    imprimirErro;
end;

procedure imprimirAjuda;
begin
  TConsole.escreverLinha('use fractais [ajuda] [config > config.txt] [< config.txt]');
  TConsole.escreverLinha();
  TConsole.escreverLinha('    ajuda                 imprimi as informa��es de uso');
  TConsole.escreverLinha('    config > config.txt   gera o arquivo de configura��o');
  TConsole.escreverLinha('    < config.txt          gera o fractal com base no arquivo de configura��o');
end;

procedure imprimirErro;
begin
  TConsole.escreverLinha('Op��es de entrada desconhecidos.');
  TConsole.escreverLinha('Use a op��o "ajuda" para mais informa��es.');
end;

end.
